const { CountryService } = require('./src/country/country.service')
const util = require('util')


function main() {
  const [key, value] = (process.argv[2] || '').slice(2).split('=')

  let result = null
  switch (key) {
    case 'filter':
      result = CountryService.filterByAnimals(value)
      break
    case 'count':
      result = CountryService.getCount()
      break
    default:
      throw new Error('arg must be filter or count')
  }

  console.log(util.inspect(result, { showHidden: false, depth: null }))
}

main()
