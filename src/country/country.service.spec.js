const { CountryService } = require('./country.service')

describe('CountryService', () => {
  describe('filterByAnimals', () => {
    it('should return countries filter by animal partial name', () => {
      const partialName = 'ry'
      const expectedResult = [
        {
          name: 'Uzuzozne',
          people: [
            {
              name: 'Lillie Abbott',
              animals: [
                {
                  name: 'John Dory',
                },
              ],
            },
          ],
        },
        {
          name: 'Satanwi',
          people: [
            {
              name: 'Anthony Bruno',
              animals: [
                {
                  name: 'Oryx',
                },
              ],
            },
          ],
        },
      ]

      const result = CountryService.filterByAnimals(partialName)

      expect(result).toEqual(expectedResult)
    })
  })

  describe('getCount', () => {
    it('name should contain children count', () => {
      const result = CountryService.getCount()

      // country count
      expect(
        result.every(({ name, people }) => name.includes(`[${people.length}]`))
      ).toBeTruthy()
      // people count
      expect(
        result.every(({ people }) =>
          people.every(({ name, animals }) =>
            name.includes(`[${animals.length}]`)
          )
        )
      )
    })
  })
})
