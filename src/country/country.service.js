const { data } = require("../../data")

class CountryService {
    static filterByAnimals(partialName) {
        return data.reduce((countries, { name: countryName, people }) => {
            const peoples = people.reduce((acc, { name: peopleName, animals }) => {
                const filteredAnimals = animals.filter(({ name }) => name.includes(partialName))

                if (!filteredAnimals.length) {
                    return acc
                }

                return [...acc, { name: peopleName, animals: filteredAnimals }]
            }, [])

            if (!peoples.length) {
                return countries
            }

            return [...countries, { name: countryName, people: peoples }]
        }, [])
    }

    static getCount() {
        return data.map(({ name: countryName, people }) => {
            const peoples = people.map(({ name, animals }) => ({ name: `${name} [${animals.length}]`, animals }))
            return { name: `${countryName} [${people.length}]`, people: peoples }
        })
    }
}

module.exports = { CountryService }